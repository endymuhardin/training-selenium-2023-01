package com.muhardin.endy.belajar.selenium.sdmdigital;

import java.net.URL;
import java.time.Duration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.muhardin.endy.belajar.selenium.SeleniumTestsConstants;
import com.muhardin.endy.belajar.selenium.sdmdigital.pageobject.LoginPage;

public class LoginTests {

	public WebDriver initWebdriver() throws Exception {
		FirefoxOptions options = new FirefoxOptions();
		if(SeleniumTestsConstants.HEADLESS) {
			options = options.addArguments("-headless");
		}
		WebDriver webDriver;

        if(SeleniumTestsConstants.REMOTE){
            webDriver = new RemoteWebDriver(new URL("http://selenium__standalone-firefox:4444"), options);
        } else {
            webDriver = new FirefoxDriver(options);
        }

		webDriver.manage().timeouts().implicitlyWait(Duration.ofMillis(3 * 1000));
        webDriver.manage().window().maximize();
        return webDriver;
	}

	public void closeWebDriver(WebDriver webDriver) throws InterruptedException{
		pauseInSeconds(5);
		webDriver.quit();
	}

    private void pauseInSeconds(Integer secs) throws InterruptedException{
        if(!SeleniumTestsConstants.HEADLESS){
			Thread.sleep(secs * 1000);
		}
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/user-login.csv", numLinesToSkip = 1)
    public void testLogin(String username, String password, Boolean success, String message) throws Exception {
        WebDriver webDriver = initWebdriver();

        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.login(username, password);
        Assertions.assertTrue(webDriver.getPageSource().contains(message));
        
        closeWebDriver(webDriver);
    }
}
