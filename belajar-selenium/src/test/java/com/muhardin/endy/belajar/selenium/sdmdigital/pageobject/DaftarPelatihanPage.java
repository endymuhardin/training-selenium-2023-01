package com.muhardin.endy.belajar.selenium.sdmdigital.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DaftarPelatihanPage {
    private static final String PAGE_URL = "https://presensi.sdm.stag.sdmdigital.id/pelatihan";

    @FindBy(id = "search")
    private WebElement txtCari;

    @FindBy(id = "btnSearching")
    private WebElement btnCari;

    public DaftarPelatihanPage(WebDriver wd){
        wd.get(PAGE_URL);
        PageFactory.initElements(wd, this);
    }

    public void cariPelatihanById(String id){
        txtCari.sendKeys(id);
        btnCari.click();
    }
}
