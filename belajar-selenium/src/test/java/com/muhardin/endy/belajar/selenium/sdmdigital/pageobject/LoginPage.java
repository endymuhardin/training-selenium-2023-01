package com.muhardin.endy.belajar.selenium.sdmdigital.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
    private static final String PAGE_URL = "https://presensi.sdm.stag.sdmdigital.id";

    @FindBy(name = "username")
    private WebElement txtUsername;

    @FindBy(id = "password")
    private WebElement txtPassword;

    @FindBy(id = "kc-login")
    private WebElement btnLogin;
    
    public LoginPage(WebDriver wd){
        wd.get(PAGE_URL);
        PageFactory.initElements(wd, this);
    }

    public void login(String username, String password){
        txtUsername.sendKeys(username);
        txtPassword.sendKeys(password);
        btnLogin.click();
    }
}
