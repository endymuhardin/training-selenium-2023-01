package com.muhardin.endy.belajar.selenium.sdmdigital.pageobject;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TambahSesiPage {
    private static final String PAGE_URL = "https://presensi.sdm.stag.sdmdigital.id/pelatihan/kelola-sesi/tambah/";
    private final WebDriver webDriver;

    @FindBy(name = "nama_sesi")
    private WebElement txtNamaSesi;

    @FindBy(id = "tanggal")
    private WebElement txtTanggal;

    public TambahSesiPage(WebDriver wd, String id){
        wd.get(PAGE_URL + id);
        PageFactory.initElements(wd, this);
        this.webDriver = wd;
    }

    public void isiNamaSesi(String nama){
        txtNamaSesi.sendKeys(nama);
    }

    public void tambahMateri(String materi){
        webDriver.findElement(By.xpath("//*[@id='materi_container']/div[2]/div/span/span[1]/span")).click();
        //*[@id='select2-materi-results']/li[text()='LMS Non-Metaverse']
        By selectorOpsi = By.xpath("//*[@id='select2-materi-results']/li[text()='"+materi+"']");
        WebElement opsi = webDriver.findElement(selectorOpsi);
        new WebDriverWait(webDriver, Duration.ofSeconds(5)).until(ExpectedConditions.elementToBeClickable(opsi)).click();
    }

    public void isiTanggal(){
        ((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView(true);", txtNamaSesi);
        txtTanggal.click();
        ((JavascriptExecutor)webDriver).executeScript("arguments[0].scrollIntoView(true);", txtTanggal);
        
        // xpath untuk mencari tanggal yang available
        //*[@id='kt_app_body']/div[13]/div[2]/div[1]/table//td[contains(@class, 'available')]

        List<WebElement> tanggalYangAvailable = webDriver.findElements(By.xpath("//*[@id='kt_app_body']/div[13]/div[2]/div[1]/table//td[contains(@class, 'available')]"));
        if(!tanggalYangAvailable.isEmpty()) {
            tanggalYangAvailable.get(0).click(); //klik tanggalnya
            new WebDriverWait(webDriver, Duration.ofSeconds(5))
            .until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='kt_app_body']/div[13]/div[4]/button[text()='Apply']")))
            .click();
        }
    }
}
