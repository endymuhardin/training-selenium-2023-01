package com.muhardin.endy.belajar.selenium.sdmdigital;

import java.net.URL;
import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.muhardin.endy.belajar.selenium.SeleniumTestsConstants;

public class TambahSesiTests {
    private static final String PAGE_URL = "https://presensi.sdm.stag.sdmdigital.id/pelatihan/kelola-sesi/tambah/";

	public WebDriver initWebdriver() throws Exception {
		FirefoxOptions options = new FirefoxOptions();
		if(SeleniumTestsConstants.HEADLESS) {
			options = options.addArguments("-headless");
		}
		WebDriver webDriver;

        if(SeleniumTestsConstants.REMOTE){
            webDriver = new RemoteWebDriver(new URL("http://selenium__standalone-firefox:4444"), options);
        } else {
            webDriver = new FirefoxDriver(options);
        }

		webDriver.manage().timeouts().implicitlyWait(Duration.ofMillis(3 * 1000));
        webDriver.manage().window().maximize();
        return webDriver;
	}

	public void closeWebDriver(WebDriver webDriver) throws InterruptedException{
		pauseInSeconds(5);
		webDriver.quit();
	}

    private void pauseInSeconds(Integer secs) throws InterruptedException{
        if(!SeleniumTestsConstants.HEADLESS){
			Thread.sleep(secs * 1000);
		}
    }

    @Test
    public void testTambahSesi() throws Exception{
        WebDriver webDriver = initWebdriver();

        webDriver.get(PAGE_URL);

        String username = "adminsakura@gmail.com";
		String password = "Stag1234#";
		String courseId = "5899";

        WebElement txtUsername = webDriver.findElement(By.id("username"));
        WebElement txtPassword = webDriver.findElement(By.id("password"));
        WebElement btnLogin = webDriver.findElement(By.id("kc-login"));

        new WebDriverWait(webDriver, Duration.ofSeconds(10))
        .until(ExpectedConditions.visibilityOf(txtUsername)).sendKeys(username);

        txtPassword.sendKeys(password);
        btnLogin.click();

        webDriver.get(PAGE_URL + courseId);
        // ini tidak bisa digunakan untuk dropdown yang dibuat dengan select2 (https://select2.org/)
        //WebElement weSelect = webDriver.findElement(By.id("materi"));
        //Select slMateri = new Select(weSelect);
        //slMateri.selectByVisibleText("Practitioner");

        closeWebDriver(webDriver);
    }
}
