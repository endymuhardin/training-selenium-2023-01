package com.muhardin.endy.belajar.selenium.sdmdigital;

import java.net.URL;
import java.time.Duration;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.muhardin.endy.belajar.selenium.SeleniumTestsConstants;
import com.muhardin.endy.belajar.selenium.sdmdigital.pageobject.DaftarPelatihanPage;
import com.muhardin.endy.belajar.selenium.sdmdigital.pageobject.LoginPage;
import com.muhardin.endy.belajar.selenium.sdmdigital.pageobject.TambahSesiPage;

public class KelolaSesiTests {
    

	public WebDriver initWebdriver() throws Exception{
		FirefoxOptions options = new FirefoxOptions();
		if(SeleniumTestsConstants.HEADLESS) {
			options = options.addArguments("-headless");
		}

		WebDriver webDriver;

        if(SeleniumTestsConstants.REMOTE){
            webDriver = new RemoteWebDriver(new URL("http://selenium__standalone-firefox:4444"), options);
        } else {
            webDriver = new FirefoxDriver(options);
        }
        
		webDriver.manage().timeouts().implicitlyWait(Duration.ofMillis(3 * 1000));
        webDriver.manage().window().maximize();
        return webDriver;
	}

	public void closeWebDriver(WebDriver webDriver) throws InterruptedException{
		pauseInSeconds(5);
		webDriver.quit();
	}

    @Test
    public void testTambahSesi() throws Exception{
        String username = "adminsakura@gmail.com";
		String password = "Stag1234#";
		String courseId = "5899";

        WebDriver webDriver = initWebdriver();

        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.login(username,password);

        DaftarPelatihanPage daftarPelatihanPage = new DaftarPelatihanPage(webDriver);
        daftarPelatihanPage.cariPelatihanById(courseId);

        TambahSesiPage tambahSesiPage = new TambahSesiPage(webDriver, courseId);
        tambahSesiPage.isiNamaSesi("Sesi Test Selenium");
        tambahSesiPage.tambahMateri("LMS Non-Metaverse");
        tambahSesiPage.tambahMateri("FInal Project");
        tambahSesiPage.isiTanggal();
    
        closeWebDriver(webDriver);
    }

    private void pauseInSeconds(Integer secs) throws InterruptedException{
        if(!SeleniumTestsConstants.HEADLESS){
			Thread.sleep(secs * 1000);
		}
    }
}
