package com.muhardin.endy.belajar.selenium;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BelajarSeleniumApplicationTests {

	public WebDriver initWebdriver() throws MalformedURLException{
		FirefoxOptions options = new FirefoxOptions();
		if(SeleniumTestsConstants.HEADLESS) {
			options = options.addArguments("-headless");
		}
		WebDriver webDriver;

        if(SeleniumTestsConstants.REMOTE){
            webDriver = new RemoteWebDriver(new URL("http://selenium__standalone-firefox:4444"), options);
        } else {
            webDriver = new FirefoxDriver(options);
        }
		webDriver.manage().timeouts().implicitlyWait(Duration.ofMillis(3 * 1000));
        return webDriver;
	}

	public void closeWebDriver(WebDriver webDriver) throws InterruptedException{
		pauseInSeconds(5);
		webDriver.quit();
	}

	@Test
	public void testSeleniumPage() throws Exception{
		WebDriver webDriver = initWebdriver();
		webDriver.get("https://www.selenium.dev/selenium/web/web-form.html");

        String title = webDriver.getTitle();
        assertEquals("Web form", title);

		WebElement textBox = webDriver.findElement(By.name("my-text"));
        WebElement submitButton = webDriver.findElement(By.cssSelector("button"));

        textBox.sendKeys("Selenium");
        submitButton.click();

        WebElement message = webDriver.findElement(By.id("message"));
        String value = message.getText();
        assertEquals("Received!", value);

		closeWebDriver(webDriver);
	}

	@Test
	public void testOpenRegistrationPage() throws Exception {
		WebDriver webDriver = initWebdriver();
		String url = "https://front-user.dev.dtsnet.net/";
		webDriver.get(url);

		// verifikasi page sudah terbuka
		String title = webDriver.getTitle();
		Assertions.assertEquals("Digital Talent Scholarship",title);

		// pastikan login button ada
		WebElement loginButton = webDriver.findElement(By.linkText("Login"));
		Assertions.assertNotNull(loginButton);

		// klik login button
		loginButton.click();

		// get element username dan password
		WebElement txtUsername = webDriver.findElement(By.name("email"));
		WebElement txtPassword = webDriver.findElement(By.name("password"));

		// explicit wait
		// tunggu sampai element username muncul
		WebDriverWait waitUsername = new WebDriverWait(webDriver, Duration.ofSeconds(10));
		waitUsername.until(d -> txtUsername.isDisplayed());
		
		String expectedLoginPageText = "Log In Akun";
		Assertions.assertTrue(webDriver.getPageSource().contains(expectedLoginPageText));

		// verifikasi element username dan password
		Assertions.assertTrue(txtUsername.isEnabled());
		Assertions.assertTrue(txtPassword.isEnabled());

		// isi username dan password
		String username = "user001";
		String password = "test123";
		txtUsername.sendKeys(username);
		txtPassword.sendKeys(password);

		// verifikasi pesan error
		String pesanErrorUsername = "Email yang diinput tidak valid.";
		String pesanErrorPassword = "Password harus lebih dari 8 karakter.";

		List<WebElement> listPesanError = webDriver.findElements(By.className("text-red"));
		System.out.println("Jumlah pesan error : "+listPesanError.size());

		for(WebElement w : listPesanError){
			System.out.println(w.getText());
		}

		Assertions.assertTrue(listPesanError.size() > 0);
		Assertions.assertEquals(pesanErrorUsername, listPesanError.get(1).getText());
		Assertions.assertEquals(pesanErrorPassword, listPesanError.get(3).getText());
		pauseInSeconds(10);

		closeWebDriver(webDriver);
	}

	private void pauseInSeconds(Integer secs) throws InterruptedException{
        if(!SeleniumTestsConstants.HEADLESS){
			Thread.sleep(secs * 1000);
		}
    }

}
